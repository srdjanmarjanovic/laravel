(function($){
    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });

    $('.ajax-loader').hide();
    $('#alerts').hide();

    $( document).ajaxStart(function(){
       $('.ajax-loader').show();
    });

    $( document ).ajaxStop(function(){
        $('.ajax-loader').hide();
    });

    $('.container .btn-connect').click(function(){
        $('#actorMovieConnectModal').modal({
            show: true
        });
    });

    if($('#alerts').hasClass('changed')){
        $('#alerts').show();
    }

    $('#add-btn').click(function(e){
        e.preventDefault();
        form = $('form');

        if (form.attr('id') == 'actor-create') {
            var field = $('input[name="name"]').val();
        }

        if (form.attr('id') == 'movie-create') {
            var field = $('input[name="title"]').val();
        }

        $.ajax({
            url: form.attr( "action" ),
            type: "post",
            data: form.serialize(),
            success: function(data) {
                entityAdded(data, field);
            },
            error: function(d)  {
                entityAdded(d);
            }
        });
    });

    $('#confirm-delete').on('show.bs.modal', function(e) {
        var entity = $(e.relatedTarget).data('entity');
        var path = $(e.relatedTarget).data('entity-type') + "/" + entity;

        $(this).find('.btn-ok').on('click', function(d){
            $.ajax({
                url: path,
                type: 'DELETE',
                data: entity,
                success: function(data){
                    $('#confirm-delete').modal('hide');
                    entityDeleted(data, entity, $(e.relatedTarget));
                },
                error: function(d){
                    entityDeleted(data, entity, '');
                }
            });
        });
    });

    function entityAdded(data, field){
        $("#alerts").show();
        if(data == 'OK'){
            $("#alerts").addClass('alert-success');
            $('form').remove();
            $("#alerts > .message").html('<p>' + field +' has been added successfully!</p>');
            editBtn = $('#edit-entity');
            editBtn.attr('href', editBtn.attr('href') + field + '/edit');

            $('#edit-entity').removeClass('hidden');
            $('#add-another').removeClass('hidden');
        } else {
            $("#alerts").addClass('alert-danger');
            $("#alerts > .message").html(data);
        }
    }

    function entityDeleted(data, name, relatedTarget){
        $('#alerts').show();
        if(data == 'OK'){
            $("#alerts").addClass('alert-success');
            $("#alerts > .message").html('<p>' + name +' has been deleted successfully!</p>');
            relatedTarget.closest('tr').remove();
        } else {

            $("#alerts").addClass('alert-danger');
            $("#alerts > .message").html(data);
        }
    }

    $('#actorMovieConnectModal').on('show.bs.modal', function(e){
        var movie = $(e.relatedTarget).data('movie');
        var actor = $(e.relatedTarget).data('actor');
        var roles = $('#actorMovieConnectModal input[name="roles"]');

        $.post( "/actors/connect", {action: 'list', actor: actor, movie: movie})
            .done(function(data){
                roles.val(data);
            });
        $(this).find('.btn-save').on('click', function(d){
            $.post("/actors/connect", {action: 'write', actor: actor, movie:movie, roles: roles.val()})
                .done(function(data){
                    entityConnect(data);
                    $('#actorMovieConnectModal').modal('hide');
                })
                .fail(function(data){
                    entityConnect(data);
                });
        });
    });

    function entityConnect(data){
        $('#alerts').show();
        if(data == 'OK'){
            $("#alerts").addClass('alert-success');
            $("#alerts > .message").html('<p>Roles added successfully</p>');
        } else {

            $("#alerts").addClass('alert-danger');
            $("#alerts > .message").html(data);
        }
    }

    $('#actorMovieConnectModal').on('hide.bs.modal', function(e){
        $('#actorMovieConnectModal input[name="roles"]').val()
    });

})(jQuery);