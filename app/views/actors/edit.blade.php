@extends('layouts.default')

@section('content')
    <h1>Edit {{ $actor['name']; }}</h1>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Actor details</h3>
      </div>
      <div class="panel-body">
        <div id="alerts" class="alert alert-dismissible @if(isset($message['class'])) {{ $message['class'] }} @endif" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true" >&times;</span></button>
                <div class="message">
                  @if(isset($message['text']))
                        {{ $message['text'] }}
                  @endif
                </div>
        </div>

        {{ Form::open(array('method' => 'PUT', 'route' => 'actors.update', 'class'=>'form-inline')) }}
            <div class="form-group">
                {{ Form::label('name', 'Name:') }}
                {{ Form::text('name', $actor['name'], ['class'=>'form-control', 'size'=> '100']) }}
                {{ Form::hidden('old_name', $actor['name']) }}

                {{ Form::label('born', 'Year born:') }}
                {{ Form::text('born', $actor['born'], ['class'=>'form-control', 'size'=> '4']) }}
                {{ Form::submit('Edit', ['id' => 'edit-btn', 'class'=>'form-control btn btn-success']) }}
            </div>
        {{ Form::close() }}

          <hr/>
          <div class="moviesConnect">
              <table class="table table-striped table-bordered table-condensed">
                  <thead>Connect with a movie:</thead>
                   <tbody>
                       @foreach($movies as $movie)
                        <tr>
                            <td> {{ link_to_route('movies.show', $movie['movie']['title'], [$movie['movie']['title']]) }} ({{ $movie['movie']['released'] }}) </td>
                            <td><a class="actorToMovie btn btn-primary" href="#" data-toggle="modal" data-target="#actorMovieConnectModal" data-movie="{{ $movie['movie']['title'] }}" data-actor="{{ $actor['name'] }}">Connect</a></td>
                        </tr>
                       @endforeach
                   </tbody>
              </table>
          </div>
          <hr/>
      {{ link_to_route('actors.index', 'Back to all actors', [], ['class' => 'btn btn-default pull-right']) }}
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="actorMovieConnectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{ Form::label('name', 'Roles:') }}
                        {{ Form::text('roles', '', ['class'=>'form-control', 'size'=> '100']) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-save">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@stop

