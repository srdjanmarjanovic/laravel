@extends('layouts.default')

@section('content')
    <h1>Add new Actor</h1>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Actor details</h3>
        </div>
        <div class="panel-body">
            <div id="alerts" class="alert alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <div class="message"></div>
            </div>

            {{ Form::open(array('route' => 'actors.store', 'class'=>'form-inline', 'id' => 'actor-create')) }}
            <div class="form-group">
                {{ Form::label('name', 'Name:') }}
                {{ Form::text('name', '', ['class'=>'form-control', 'size'=> '100']) }}

                {{ Form::label('born', 'Year born:') }}
                {{ Form::text('born', '', ['class'=>'form-control', 'size'=> '4']) }}
                {{ Form::submit('Add', ['id' => 'add-btn', 'class'=>'form-control btn btn-success']) }}
            </div>
            {{ Form::close() }}
            <hr/>
            <div class="controls pull-right">
                {{ link_to_route('actors.index', 'Back to all', [], ['class' => 'btn btn-default']) }}
                <a href="/actors/" id="edit-entity" class="btn btn-warning hidden">Edit actor</a>
                {{ link_to_route('actors.create', 'Add another', [], ['class' => 'btn btn-primary hidden', 'id'=>'add-another']) }}
            </div>
        </div>
    </div>



@stop

