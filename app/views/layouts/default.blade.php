<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="_token" content="{{ csrf_token() }}"/>

	<title>{{ $title }}</title>
	{{ HTML::style('/css/bootstrap.min.css') }}
    {{--{{ HTML::style('/css/bootstrap-theme.css') }}--}}
    {{ HTML::script('/js/jquery-1.11.2.min.js') }}
    {{ HTML::style('/css/style.css') }}
    <link href='https://fonts.googleapis.com/css?family=Lobster|Playball&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
       <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="/">Home</a></li>
        <li><a href="/actors">Actors</a></li>
        <li><a href="/movies">Movies</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
    <div class="ajax-loader"></div>
    @yield('header')
    <div class="container">
        @yield('content')
    </div>
    {{ HTML::script('/js/bootstrap.min.js') }}
    {{ HTML::script('/js/ajax.js') }}
</body>
</html>