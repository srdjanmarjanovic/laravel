@extends('layouts.default')

@section('content')
    <h1>Add new Movie</h1>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Movie details</h3>
        </div>
        <div class="panel-body">
            <div id="alerts" class="alert alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <div class="message"></div>
            </div>

            {{ Form::open(array('route' => 'movies.store', 'id' => 'movie-create')) }}
                <div class="form-group">
                    {{ Form::label('title', 'Title:') }}
                    {{ Form::text('title', '', ['class'=>'form-control', 'size'=> '100']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('tagline', 'Tagline:') }}
                    {{ Form::text('tagline', '', ['class'=>'form-control', 'size'=> '100']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('released', 'Year released:') }}
                    {{ Form::text('released', '', ['class'=>'form-control', 'size'=> '4']) }}
                </div>
                <div class="from-group clearfix">
                    {{ Form::submit('Add', ['id' => 'add-btn', 'class'=>'pull-right btn btn-success']) }}
                </div>
            {{ Form::close() }}
            <hr/>
            <div class="controls pull-right">
                {{ link_to_route('movies.index', 'Back to all', [], ['class' => 'btn btn-default']) }}
                <a href="/movies/" id="edit-entity" class="btn btn-warning hidden">Edit movie</a>
                {{ link_to_route('movies.create', 'Add another', [], ['class' => 'btn btn-primary hidden', 'id'=>'add-another']) }}
            </div>
        </div>
    </div>



@stop

