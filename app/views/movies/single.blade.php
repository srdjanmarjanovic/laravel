@extends('layouts.default')

@section('content')
<h1> {{ $movie['title'] }} @if(isset($movie['released'])) ({{ $movie['released'] }}) @endif</h1>
@if(isset($movie['tagline']))
    <h3><em>{{ $movie['tagline'] }}</em></h3>
@endif
<hr/>
<div class="panel panel-primary">
  <div class="panel-heading">Actors</div>
  @if(!empty($actors[0]['actor']))
      <table class="actor-movies table table-striped table-bordered table-condensed">
          <thead>
              <tr>
                  <th class="title">Name:</th>
                  <th class="roles">Roles:</th>
              </tr>
          </thead>
          <tbody>
          @foreach($actors as $actor)
              <tr>
                  <td><a href="/actors/{{ $actor['actor']['name'] }}">{{ $actor['actor']['name'] }} @if(isset($actor['actor']['born'])) ({{ $actor['actor']['born'] }}) @endif </a></td>
                  <td>
                      @foreach($actor['roles'] as $role)
                        {{ $role }}<br/>
                      @endforeach
                  </td>
              </tr>
          @endforeach
          </tbody>
      </table>
  @else
      <div class="panel-body">
          <h3>There are no actors connected to this movie. </h3>
          <p>
              Why not edit it and add some?
          </p>
      </div>
  @endif
</div>

<div class="row-same-height">
    <div class="col-xs-4 col-xs-height well">
        <p>Directed by:</p>
        <ul>
            @foreach($directors as $director)
                <li>{{ $director['name'] }} ({{ $director['born'] }})</li>
            @endforeach
        </ul>
    </div>
    <div class="col-xs-4 col-xs-height well">
        <p>Produced by:</p>
        <ul>
            @foreach($producers as $producer)
                <li>{{ $producer['name'] }} ({{ $producer['born'] }})</li>
            @endforeach
        </ul>
    </div>
    <div class="col-xs-4 col-xs-height well">
        <p>Written by:</p>
        <ul>
            @foreach($writers as $writer)
                <li>{{ $writer['name'] }} ({{ $writer['born'] }})</li>
            @endforeach
        </ul>
    </div>
</div>
<hr/>
{{ link_to_route('movies.edit', 'Edit', [$movie['title']], ['class' => 'btn btn-xs btn-warning']) }}


@stop