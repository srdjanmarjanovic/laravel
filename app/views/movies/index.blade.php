@extends('layouts.default')

@section('content')
    <br/>
    {{ link_to_route('movies.create', 'Create new', [], ['class' => 'btn btn-primary pull-right']) }}

    <h1>Movies</h1>
    <div id="alerts" class="alert alert-dismissible @if(isset($message['class'])) {{ $message['class'] }} @endif" role="alert">
            <div class="message"></div>
        </div>
    <table class="movies table table-striped table-condensed">
        <thead>
            <tr>
                <th class="title">Title:</th>
                <th class="tagline">Tagline:</th>
                <th class="year">Actors:</th>
                <td colspan="2"></td>

            </tr>
        </thead>
        <tbody>
        @foreach($movies as $item)
            <tr>
               <td class="name">
                    <a href="/movies/{{$item['movie']['title']}}">
                        {{ $item['movie']['title'] }}
                        @if(isset($item['movie']['released']))
                           ({{ $item['movie']['released'] }})
                        @else
                            N/A
                        @endif
                    </a>
               </td>
               <td class="tagline">
                    @if(isset($item['movie']['tagline']))
                        {{ $item['movie']['tagline'] }}
                    @else
                        N/A
                    @endif
               </td>
               <td class="released">
                   @if(isset($item['actorsNum']))
                       {{ $item['actorsNum'] }}
                   @endif
               </td>
               <td>
                   {{ link_to_route('movies.edit', 'Edit', [$item['movie']['title']], ['class' => 'btn btn-xs btn-warning']) }}
               </td>
               <td>
                   <a href="#" class="btn btn-xs btn-danger delete-btn" data-toggle="modal" data-target="#confirm-delete" data-entity-type="movies" data-entity="{{ $item['movie']['title'] }}">Delete</a>
               </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Confirm Delete
                </div>
                <div class="modal-body">
                   <p>You are about to delete an actor, this procedure is irreversible.</p>
                    <p>Do you want to proceed?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>

@stop