@extends('layouts.default')
@section('header')
    <header>
        <div class="header-mask"></div>
        <div class="container">
            <h1>Movies DB</h1>
            <p>World's first movies graph database</p>
        </div>
    </header>
@stop