<?php

class Actor extends srdjan\neo4l\Neo4l {
  protected $fillable = [];

  /**
   * Get all actors and number of the roles they played
   * @return array
   */
  public function all() {
    $query = $this->client->query("MATCH (actor:Person)
                                      OPTIONAL MATCH (actor)-[role:ACTED_IN]->(movie:Movie)
                                      WITH length(role.roles) as roleCount, actor
                                      RETURN sum(roleCount) as totalRoleCount, actor
                                      ORDER BY totalRoleCount DESC;");
    $results = [];
    foreach ($query as $key => $row) {
      $results[$key]['actor'] = $row['actor'];
      $results[$key]['totalRoleCount'] = $row['totalRoleCount'];
    }

    return $results;
  }

  /**
   * Get actor details by his/hers name,
   * @param str $name
   * @return array 'movies', 'coactors'
   */
  public function single($name) {
    $results = [];

    // Get actor details, coactors, movies in which they coacted and order by time they coacted
    $query = $this->client->query('MATCH (actor:Person{name:"' . $name . '"})-[role:ACTED_IN]->(movies)<-[r:ACTED_IN]-(coactors)
                                       WITH actor, coactors, count(coactors) as TimesCoacted, collect(DISTINCT movies) as movies
                                       RETURN movies, coactors.name, avg(TimesCoacted) as TimesCoacted
                                       ORDER BY TimesCoacted DESC');

    $coactors = [];
    foreach ($query as $key => $row) {
      $coactors[] = $row;
    }

    //Unset the $query to reuse it
    unset($query);

    //Get movies for actor, along with all roles he or she played in it
    $query = $this->client->query('MATCH (actor:Person {name:"' . $name . '"})-[role:ACTED_IN]->(movies)
                                       WITH actor, movies, role
                                       ORDER BY movies.released DESC
                                       RETURN actor, collect({roles: role.roles, movies: movies}) as movies');
    $movies = [];
    foreach ($query as $key => $row) {
      $movies[] = $row;
    }

    $results['movies'] = $movies;
    $results['coactors'] = $coactors;

    return $results;
  }

  /**
   * Create new actor
   * @param array $vars 'name' and 'born'
   * @return array
   */
  public function create_new($vars) {
    $name = $vars['name'];
    $born = $vars['born'];

    $query = $this->client->query('CREATE (actor:Person{name:"' . $name . '", born:"' . $born . '"})');
    return array('code' => $query->errorCode(), 'info' => $query->errorInfo());
  }

  /**
   * Get basic info for an actor
   * @param str $name
   * @return array
   */
  public function info($name) {
    $results = $this->client->query('MATCH (actor:Person{name:"' . $name . '"})
                                         RETURN actor');
    foreach ($results as $row) {
      $actor['name'] = $row['actor']['name'];
      $actor['born'] = (!empty($row['actor']['born'])) ? $row['actor']['born'] : '';
    }

    return $actor;
  }

  /**
   * Edit an actor
   * @param array $data 'old_name' to alter with the 'new_name'
   * @return array
   */
  public function edit($data) {
    $query = $this->client->query('MATCH (actor:Person{name:"' . $data['old_name'] . '"})
                                   SET actor.name = "' . $data['new_name'] . '"
                                   SET actor.born = "'.$data['born'].'"
                                   RETURN actor');
    return array('code' => $query->errorCode(), 'info' => $query->errorInfo());
  }

  /**
   * Delete an actor and all his/hers relationships, if it has any
   * @param str $name
   * @return array
   */
  public function delete($name) {
    $query = $this->client->query('MATCH (actor{name:"' . $name . '"})
                                   OPTIONAL MATCH (actor)-[r]-()
                                   DELETE actor, r');
    return array('code' => $query->errorCode(), 'info' => $query->errorInfo());
  }
}