<?php

class ActorsController extends BaseController {

  /**
   * The layout that should be used for responses.
   */
  protected $layout = 'layouts.default';
  protected $actor;
  protected $movie;

  public function __construct(Actor $actor, Movie $movies) {
    $this->actor = $actor;
    $this->movie = $movies;
  }

  /**
   * Display a listing of the resource.
   * GET /actors
   *
   * @return Response
   */
  public function index() {
    $actors = $this->actor->all();
    return View::make('actors.index', compact('actors'))
      ->withTitle("MovieDB - Actors")
      ->withMessage(Session::get('message'));
  }

  /**
   * Show the form for creating a new resource.
   * GET /actors/create
   *
   * @return Response
   */
  public function create() {
    return View::make('actors.create')
      ->withTitle("MovieDB - Create new actor");
  }

  /**
   * Store a newly created resource in storage.
   * POST /actors
   *
   * @return Response
   */
  public function store() {
    //Check if request is indeed ajax
    if (Request::ajax()) {

      //Try to add new record
      $result = $this->actor->create_new(Input::all());

      //Check if there were any errors
      if ($result['code'] == '00000') {
        //Just return that everything is OK
        return 'OK';
      }
      else {
        //If there were, return error info:
        return 'Error code: ' . $result['code'] . "<br>" .
        'Error info: ' . $result['info'];
      }
    }
  }

  /**
   * Display the specified resource.
   * GET /actors/{name}
   *
   * @param  str $name
   * @return Response
   */
  public function show($name) {
    $result = $this->actor->single($name);

    $actor = reset($result['movies'])['actor'];

    foreach ($result['coactors'] as $key => $item) {
      $coactors[$key]['name'] = $item['coactors.name'];
      $coactors[$key]['movies'] = $item['movies'];
      $coactors[$key]['times-coacted'] = $item['TimesCoacted'];
    }

    foreach (reset($result['movies'])['movies'] as $key => $item) {
      $movies[$key]['title'] = $item['movies']['title'];
      $movies[$key]['tagline'] = !empty($item['movies']['tagline']) ? $item['movies']['tagline'] : '';
      $movies[$key]['released'] = $item['movies']['released'];
      $movies[$key]['roles'] = $item['roles'];
    }

    return View::make('actors.single')
      ->withActor($actor)
      ->withCoactors($coactors)
      ->withMovies($movies)
      ->withTitle("MovieDB - {$name}");
  }

  /**
   * Show the form for editing the specified resource.
   * GET /actors/{$name}/edit
   *
   * @param  string $name
   * @return Response
   */
  public function edit($name) {
    $actor = $this->actor->info($name);
    $movies = $this->movie->all();
    return View::make('actors.edit')
      ->withTitle('Movies DB - Edit ' . $actor['name'])
      ->withActor($actor)
      ->withMovies($movies)
      ->withMessage(Session::get('message'));
  }

  /**
   * Update the specified resource in storage.
   * PUT /actors/{old_name}/{name}
   *
   * @return Response
   */
  public function update() {
    $old_name = Input::get('old_name');
    $new_name = Input::get('name');
    $born = Input::get('born');
    //Try to edit record
    $result = $this->actor->edit([
      'old_name' => $old_name,
      'new_name' => $new_name,
      'born' => $born,
    ]);

    //Check if there were any errors
    if ($result['code'] == '00000') {
      $message['text'] = 'Actor edited successfully!';
      $message['class'] = 'alert-success changed';
      //Redirect to the new name actor page
      return Redirect::route('actors.edit', [$new_name])->withMessage($message);
    }
    else {
      //If there were, return error info:
      $message['text'] = 'There was an error!<br>';
      $message['text'] .= 'Error code: ' . $result['code'] . "<br>";
      $message['text'] .= 'Error info: ' . $result['info'];
      $message['class'] = 'alert-danger changed';
      return Redirect::route('actors.edit', [$old_name])->withMessage($message);
    }
  }

  /**
   * Remove the specified resource from storage.
   * DELETE /actors/{$name}
   *
   * @param  string $name
   * @return Response
   */
  public function destroy($name) {
    $result = $this->actor->delete($name);
    //Check if there were any errors
    if ($result['code'] == '00000') {
      $message = 'OK';
    }
    else {
      //If there were, return error info:
      $message = 'There was an error!<br>';
      $message .= 'Error code: ' . $result['code'] . "<br>";
      $message .= 'Error info: ' . $result['info'];
    }

    return $message;
  }


  /**
   * Helper function to get or set actors roles in a movie
   *
   * @return string
   */
  public function postConnect() {
    if (Request::ajax()) {
      switch (Input::get('action')) {
        case 'list':
          $results = $this->movie->getActorRolesInMovie(Input::get('actor'), Input::get('movie'));
          if ($results['roles']) {
            return implode(', ', $results['roles']);
          }
          else {
            return [];
          }
          break;
        case 'write':
          $result = $this->movie->setActorRolesInMovie(Input::get('actor'), Input::get('movie'), Input::get('roles'));

          //Check if there were any errors
          if ($result['code'] == '00000') {
            //Just return that everything is OK
            return 'OK';
          }
          else {
            //If there were, return error info:
            return 'Error code: ' . $result['code'] . "<br>" .
            'Error info: ' . $result['info'];
          }
          break;
        default:
          return 'no method';
          break;
      }
    }
  }

}